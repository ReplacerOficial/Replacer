using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using ElectronNET.API;
using ElectronNET.API.Entities;
using MariGlobals.Logger.General;
using MariGlobals.Utils;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Hosting.Server.Features;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Replacer
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var debug = false;
            Debug.Assert(debug = true);

            services.AddLogging((logging) =>
            {
                logging.ClearProviders();
                logging.SetMinimumLevel(LogLevel.Trace);
                logging.AddMariLogger(Configuration, options =>
                {
                    if (debug)
                        options.EnableWriter = true;
                });
            });

            services.AddControllers();
            services.AddSpaStaticFiles(options =>
            {
                options.RootPath = "front-end/dist";
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseStaticFiles();
            app.UseSpaStaticFiles();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseSpa((spa) =>
            {
                spa.Options.SourcePath = "front-end";

                if (env.IsDevelopment())
                    spa.UseProxyToSpaDevelopmentServer($"http://localhost:{GetVuePort()}");
            });

            Task.Run(() => CreateWindowAsync(env.IsDevelopment()));
        }

        private async Task CreateWindowAsync(bool debug)
        {
            var window = await Electron.WindowManager.CreateWindowAsync(new BrowserWindowOptions
            {
                WebPreferences = new WebPreferences
                {
                    NodeIntegration = false,
                    DevTools = debug
                },
                Title = "Replacer",
                Icon = GetApplicationIcon(),
                Show = false,
                
            });

            if (!debug)
                window.RemoveMenu();
                
            window.Maximize();
            window.OnReadyToShow += () => window.Show();
        }

        private string GetApplicationIcon()
        {
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                return $"{Directory.GetCurrentDirectory()}/Assets/icon.ico";
            }
            else if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
            {
                return $"{Directory.GetCurrentDirectory()}/Assets/icon-linux.png";
            }
            else if (RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
            {
                return $"{Directory.GetCurrentDirectory()}/Assets/icon-mac.png";
            }

            return $"{Directory.GetCurrentDirectory()}/Assets/icon-linux.png";
        }

        private int GetVuePort()
            => int.Parse(Environment.GetEnvironmentVariable("VUE_PORT"));
    }
}