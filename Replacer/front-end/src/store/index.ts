import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    messages: Object
  },
  mutations: {
    SET_MESSAGES(store, obj) {
      store.messages = obj.messages;
    }
  },
  getters: {
    messagesLoad: state => {
      return state.messages;
    }
  },
  actions: {},
  modules: {}
});
