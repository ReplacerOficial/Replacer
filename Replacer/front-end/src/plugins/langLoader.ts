import Vue from "vue";
import pt from "../messages/pt-br.json";
import en from "../messages/en-gb.json";

export const loadAsync = async (vue: Vue): Promise<void> => {
  let selectedLanguage = getAndUpdateLanguage(vue);
  const jsonLanguage = getJsonLanguage(selectedLanguage);

  vue.$store.commit("SET_MESSAGES", {
    messages: jsonLanguage
  });
};

const isValidLanguage = (vue: Vue, language: string): boolean => {
  return hasProperty(vue.$vuetify.lang.locales, language);
};

const getJsonLanguage = (language: string): object => {
  if (language === "en") {
    return en;
  } else {
    return pt;
  }
};

const getAndUpdateLanguage = (vue: Vue): string => {
  const defaultLanguage = vue.$vuetify.lang.current;
  let selectedLanguage = defaultLanguage;

  if (localStorage.language && isValidLanguage(vue, localStorage.language)) {
    selectedLanguage = localStorage.language;
  }

  localStorage.language = selectedLanguage;

  return selectedLanguage;
};

export const waitLanguageLoadAsync = async (vue: Vue): Promise<void> => {
  await vue.$store.getters.messagesLoad;
};

export const changeLanguageAsync = async (
  vue: Vue,
  language: string
): Promise<boolean> => {
  if (!isValidLanguage(vue, language)) {
    return false;
  } else {
    localStorage.language = language;
    await loadAsync(vue);
    return true;
  }
};

const hasProperty = (obj: any, prop: any): boolean => {
  return obj[prop] !== undefined;
};

// export const getMessageOrLoading = (vue: Vue, message: string): string => {
//   if (vue.$data.isLoadingPage === true) {
//     const selectedLanguage = getAndUpdateLanguage(vue);
//     return loadingMessages.get(selectedLanguage) as string;
//   } else {
//     return getMessageToken(vue, message);
//   }
// };

// export const getMessageToken = (vue: Vue, message: string): string => {
//   message = message.replace(/\[["'`](.*)["'`]\]/g, ".$1");

//   return message.split(".").reduce((prev, curr) => {
//     return prev ? prev[curr] : undefined;
//   }, vue.$store.state.messages || self);
// };

// const loadingMessages = new Map<string, string>([
//   ["pt", "Carregando"],
//   ["en", "Loading"]
// ]);
