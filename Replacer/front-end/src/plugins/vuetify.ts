import Vue from "vue";
import Vuetify from "vuetify";
import "vuetify/dist/vuetify.min.css";
import pt from "vuetify/src/locale/pt";
import en from "vuetify/src/locale/en";

Vue.use(Vuetify);

export default new Vuetify({
  lang: {
    locales: {
      pt,
      en
    },
    current: "pt"
  },
  icons: {
    iconfont: "fa"
  },
  theme: {
    dark: true
  }
});
