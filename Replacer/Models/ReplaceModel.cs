using System.ComponentModel.DataAnnotations;

namespace Replacer.Models
{
    public class ReplaceModel
    {
        [Required]
        public string Pattern { get; set; }

        [Required]
        public string Replaces { get; set; }

        [Required]
        public string Replace { get; set; }
    }
}