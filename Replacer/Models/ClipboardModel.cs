using System.ComponentModel.DataAnnotations;

namespace Replacer.Models
{
    public class ClipboardModel
    {
        [Required]
        public string Text { get; set; }
    }
}