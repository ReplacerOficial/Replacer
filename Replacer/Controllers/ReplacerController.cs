using MariGlobals.Utils;
using Microsoft.AspNetCore.Mvc;
using Replacer.Models;

namespace Replacer.Controllers
{
    public class ReplacerController : BaseController
    {
        [HttpPost]
        public IActionResult ExecuteReplace(ReplaceModel replaceModel) 
        {
            if (!TryValidateModel(replaceModel))
                return BadRequest();

            var replaces = replaceModel.Replaces.Replace(" ", string.Empty).Split(',');
            var result = string.Empty;

            foreach (var replace in replaces) 
            {
                result += $"{replaceModel.Replace.Replace(replaceModel.Pattern, replace)}\n";
            }

            return Ok(new 
            {
                result
            });
        }        
    }
}