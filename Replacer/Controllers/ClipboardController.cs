using ElectronNET.API;
using MariGlobals.Utils;
using Microsoft.AspNetCore.Mvc;
using Replacer.Models;

namespace Replacer.Controllers
{
    public class ClipboardController : BaseController
    {
        [HttpPost]
        public IActionResult CopyToCliboard([FromBody]ClipboardModel clipboard) 
        {
            if (!TryValidateModel(clipboard))
                return BadRequest();

            Electron.Clipboard.WriteText(clipboard.Text);

            return Ok();
        }
    }
}